import json

'''
    404: 未找到
    200: 成功返回
    3xx: 重定向
    5xx: 服务器错误
'''


class ResponseStatus(object):
    def __init__(self, code, message):
        self.code = code
        self.message = message

    def to_json(self):
        dic = self.__dict__  # 拿到自己所有的成员对象字典
        str_json = json.dumps(dic, ensure_ascii=False)  # 把自己所有的成员对象字典转化为JSON

        return str_json

    def error(self, error_msg):
        self.code = 555
        self.message = error_msg
        return self.to_json()