import json


class Poem(object):
    def __init__(self, status, content):
        self.status = status
        self.content = content

    def to_json(self):
        dic = self.__dict__
        return json.dumps(dic, ensure_ascii=False)