from flask import Flask, request
from flask_cors import CORS
import sys

from sts.sts import Sts

from database import Database
from photo import *

sys.path.append('../codes')
sys.path.append('../../entities')

from keys2poem.codes.generate import Keys2Poem
from text2poem.generator import Text2Poem
from write4u.generator import ContinuePoem
from entities.poem import Poem
from entities.response_status import ResponseStatus

app = Flask(__name__)
CORS(app, resource=r'/*')


@app.route('/keys2poem', methods=['POST', 'GET'])
def key2poem():
    keywords = request.form.get('keywords')
    num_per_poem = request.form.get('num_per_poem')
    label1 = request.form.get('label1')
    label2 = request.form.get('label2')
    print(keywords, num_per_poem)
    demo = Keys2Poem(str(keywords), int(num_per_poem), int(label1), int(label2))
    content = demo.gen_poem()
    return Poem(num_per_poem, content).to_json()


@app.route('/nextPoem', methods=['POST'])
def next_poem():
    text = request.form.get('text')
    print(text)
    demo = ContinuePoem(text)
    content = demo.gen_poem()
    poem = []
    for item in content:
        str_poem = ''.join(item)
        poem.append(str_poem)
    return Poem('qq', poem).to_json()


@app.route('/text2poem', methods=['GET', "POST"])
def text2poem():
    content = request.form.get('content')
    num_per_poem = request.form.get('num_per_poem')
    label1 = request.form.get('label1')
    label2 = request.form.get('label2')
    demo = Text2Poem(content)
    result_list = [str(word) for word in demo.extract_keys()]
    result = " ".join(result_list)
    demo = Keys2Poem(result, int(num_per_poem), int(label1), int(label2))
    content = demo.gen_poem()
    return Poem(num_per_poem, content).to_json()


@app.route('/img2poem', methods=['POST'])
def img2poem():
    # img 为 base64.b64encode() 编码过的内容
    which_front = request.form.get("which_front")
    if which_front == 1:
        image = request.files['image']
        img = base64.b64encode(image.read())
        num_per_poem = request.form.get('num_per_poem')
    else:
        img = request.form.get("image")
        num_per_poem = request.form.get('num_per_poem')

    # app_id = 26585635
    def __get_access_token():
        api_key = 'Sy8zRD9DIsOoNXag3zOkwNfw'
        secret_key = 'r60GYbtcMBo8hNrUcBRFxto2GCfZn54T'

        token_baseurl = 'https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id={}&client_secret={}' \
            .format(api_key, secret_key)
        response = requests.get(token_baseurl)
        access_token = ''
        if response:
            # print(response.json())
            access_token = response.json()['access_token']
        return access_token

    # img = base64.b64encode()
    def __get_img_key(image):
        params = {"image": image}
        baseurl = 'https://aip.baidubce.com/rest/2.0/image-classify/v2/advanced_general'
        request_url = baseurl + "?access_token={}".format(__get_access_token())
        headers = {'content-type': 'application/x-www-form-urlencoded'}
        response = requests.post(request_url, data=params, headers=headers)
        # print(response)
        result = response.json()['result']
        keyword = [x['keyword'] for x in result[0:4]]
        # print(keyword)
        for key in keyword:
            if len(key) > 2:
                continue
            else:
                keyword = ' '.join(keyword)
        return keyword

    demo = Keys2Poem(__get_img_key(img), int(num_per_poem), -1, -1)
    content = demo.gen_poem()
    return Poem(num_per_poem, content).to_json()


@app.route("/AI_poem_create", methods=["GET", "POST"])
def select():
    if request.method == "GET":
        return 0
    else:
        # 接收用户通过post形式提交的数据
        print(request.form)
        select_num = request.form.get("lengthType")
        # 判断诗的长度
        select_mtd = request.form.get("methodType")
        key = request.form.get("message")
        # 进行判断key值，调用算法
        # {
        # }
        print(select_num, select_mtd, key)
        return "等待算法"


@app.route('/auth', methods=['GET'])
def get_temp_token():
    config = {
        'url': 'https://sts.tencentcloudapi.com/',
        # 域名，非必须，默认为 sts.tencentcloudapi.com
        'domain': 'sts.tencentcloudapi.com',
        # 临时密钥有效时长，单位是秒
        'duration_seconds': 1800,
        'secret_id': 'AKID356CMQRQQFvWzGLF3hAUegW9eO4fBbCg',
        # 固定密钥
        'secret_key': 'UrBZuAZpx1vfJrPpHnaOESeunjaL3qfQ',
        # 换成你的 bucket
        'bucket': 'ai-poem-1312743943',
        # 换成 bucket 所在地区
        'region': 'ap-nanjing',
        # 这里改成允许的路径前缀，可以根据自己网站的用户登录态判断允许上传的具体路径
        # 例子： a.jpg 或者 a/* 或者 * (使用通配符*存在重大安全风险, 请谨慎评估使用)
        'allow_prefix': 'ai-poem/*',
        # 密钥的权限列表。简单上传和分片需要以下的权限，其他权限列表请看 https://cloud.tencent.com/document/product/436/31923
        'allow_actions': [
            # 简单上传
            'name/cos:PutObject',
            'name/cos:PostObject',
            # 分片上传
            'name/cos:InitiateMultipartUpload',
            'name/cos:ListMultipartUploads',
            'name/cos:ListParts',
            'name/cos:UploadPart',
            'name/cos:CompleteMultipartUpload',
            # 简单下载
            'name/cos:GetObject'
        ],
    }
    try:
        sts = Sts(config)
        response = sts.get_credential()
        return response
    except Exception as e:
        print(e)


@app.route("/AI_poem_create/postFont", methods=["POST"])
def show():
    if request.method == "POST":
        print(request.form)
        database = Database()
        image_bk = request.form.get("picture")
        poem = request.form.get("poem").split(',')
        font = request.form.get("font")
        color = request.form.get("color")
        length = len(poem[0])  # 五言或七言
        pic_url = file_load("c%s.png" % image_bk, font, color, poem, length)
        database.insert_image(pic_url, 0)
        del database
        return pic_url


@app.route("/AI_poem_create/get_Top", methods=["GET", "POST"])
def top():
    if request.method == "POST":
        database = Database()
        cardList = database.get_best_card()
        return_list = json.dumps(cardList, cls=MyEncoder, indent=4)
        del database
        return return_list


@app.route("/AI_poem_create/get_new", methods=["GET", "POST"])
def new():
    if request.method == "POST":
        database = Database()
        cardList = database.get_new_card()
        return_list = json.dumps(cardList, cls=MyEncoder, indent=4)
        del database
        return return_list


@app.route("/AI_poem_create/get_random", methods=["POST"])
def random():
    if request.method == "POST":
        database = Database()
        cardList = database.get_random_card()
        return_list = json.dumps(cardList, cls=MyEncoder, indent=4)
        del database
        return return_list


# @app.route("/AI_poem_create/get_base", methods=["POST"])
# def getBase():
#     if request.method == "POST":
#         base64_url = request.form.get("url")
#         img_stream = get_base(base64_url)
#         return img_stream


@app.route("/AI_poem_create/update_like", methods=["POST"])
def update():
    if request.method == "POST":
        database = Database()
        result = database.update_num(request.form.get("id"), request.form.get("like"))
        del database
        return result


if __name__ == '__main__':
    app.run(host="0.0.0.0", port=5000)
