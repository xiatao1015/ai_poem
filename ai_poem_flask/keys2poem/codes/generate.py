from generator import Generator


class Keys2Poem(object):
    def __init__(self, keyword, num_per_poem, label1, label2):
        self.keyword = keyword
        self.num_per_poem = num_per_poem
        '''
        specify the living experience label
            0: military career, 1: countryside life, 2: other:, -1: not specified
        '''
        self.label1 = label1
        '''
        specify the historical background label
            0: prosperous times, 1: troubled times, -1: not specified
        '''
        self.label2 = label2

    def gen_poem(self):
        instance = Generator()
        print(self.keyword, self.num_per_poem, self.label1, self.label2)
        poem, info = instance.generate_one(self.keyword, self.num_per_poem,
                                           self.label1, self.label2)
        if len(poem) != 4:
            print(info)
            return 'failed'
        else:
            return poem

# if __name__ == "__main__":
#     demo = Keys2Poem('情 折扇 雪花', 7, 1, 0)
#     print(demo.gen_poem())
