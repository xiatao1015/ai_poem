import random

import paddlehub as hub


class ContinuePoem(object):
    def __init__(self, text):
        self.text = [text]

    def __gen_next(self, input):
        module = hub.Module(name="ernie_gen_poetry")  # 调用古诗生成的模型
        num_per_poem = len(input[0])
        results = module.generate(texts=input, use_gpu=False, beam_width=5)
        SecondPoetry = ''
        assert num_per_poem in {5, 7}
        i = random.randint(0, len(results))
        if num_per_poem == 5:
            SecondPoetry = [results[0][i][0:5]]  # 增加随机性
        elif num_per_poem == 7:
            SecondPoetry = [results[0][i][0:7]]  # 增加随机性
        return SecondPoetry

    def gen_poem(self):
        second_poem = self.__gen_next(self.text)
        third_poem = self.__gen_next(second_poem)
        last_poem = self.__gen_next(third_poem)
        poetrys = []
        poetrys.append(self.text)
        poetrys.append(second_poem)
        poetrys.append(third_poem)
        poetrys.append(last_poem)
        for poem in poetrys:
            print(poem)
        return poetrys

    # def __get_img_cls(self):
    #     module_image = hub.Module(name="resnet_v2_152_imagenet")  # 调用图像分类的模型
    #
    #     test_img_path = self.image  # 选择图像，即要根据哪张图片写诗
    #
    #     # set input dict
    #     input_dict = {"image": [test_img_path]}
    #
    #     # execute predict and print the result
    #     results_image = module_image.classification(data=input_dict)
    #     print(results_image)
    #
    #     img_cls = list(results_image[0][0].keys())[0]
    #     # print("该图片的分类是：", img_cls)  # 得到分类结果
    #
    #     translator = Translator(to_lang='chinese')
    #     img_cls_ch = translator.translate("{}".format(img_cls))
    #     print(img_cls_ch)
    #     return img_cls_ch
    #
    # def __gen_couplet(self):
    #     module_similar = hub.Module(name="ernie_gen_couplet")  # 调用对联生成的模型
    #     texts = ['{}'.format(self.__get_img_cls())]
    #     results = module_similar.generate(texts=texts, use_gpu=False, beam_width=20)
    #     Words = []  # 将符合标准的近义词保存在这里（标准：字符串为中文且长度大于1）
    #
    #     for item in range(20):
    #         # print(results[0][item])
    #         if self.__is_chinese(results[0][item]):
    #             Words.append(results[0][item])
    #     # print(Words)
    #     keys = np.random.randint(0, 16, size=8)  # 使生成的古诗具有多样性
    #     # 古诗的一句可以拆分成许多词语，因此这里先找到能合成古诗的词语
    #     FirstWord = Words[keys[0]] + Words[keys[1]]
    #     SecondWord = Words[keys[2]] + Words[keys[3]]
    #     ThirdWord = Words[keys[4]] + Words[keys[5]]
    #     FourthWord = Words[keys[6]] + Words[keys[7]]
    #
    #     # 出句和对句，也可以理解为上下句（专业讲法是出句和对句，古诗词是中国传统文化，出句和对句的英文翻译即拼音）
    #     ChuJu = FirstWord + SecondWord  # 出句
    #     DuiJu = ThirdWord + FourthWord  # 对句
    #     assert self.num_per_poem in {5, 7}
    #     if self.num_per_poem ==5:
    #         first_poetry = ["{:.5}，{:.5}。".format(ChuJu, DuiJu)]  # 古诗词的上阕
    #     elif self.num_per_poem == 7:
    #         first_poetry = ["{:.7}，{:.7}。".format(ChuJu, DuiJu)]  # 古诗词的上阕
    #     return first_poetry

    # def __is_chinese(self, string):
    #     """
    #     检查整个字符串是否为中文
    #     Args:
    #         string (str): 需要检查的字符串,包含空格也是False
    #     Return
    #         bool
    #     """
    #     if len(string) <= 1:  # 去除只有单个字或者为空的字符串
    #         return False
    #
    #     for chart in string:  # 把除了中文的所有字母、数字、符号去除
    #         if chart < u'\u4e00' or chart > u'\u9fff':
    #             return False
    #     return True


if __name__ == '__main__':
    demo = ContinuePoem('昔年旅南服')
    demo.gen_poem()
