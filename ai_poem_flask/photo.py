import base64
import io
import json
import os
import re
import urllib.request
import numpy as np
import requests
from PIL import ImageFont, Image, ImageDraw
from io import BytesIO
import uuid
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client

base_url = "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/bam/"


class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()
        elif isinstance(obj, bytes):
            return str(obj, encoding='utf-8')
        return json.JSONEncoder.default(self, obj)


def cos_upload(image_path, file_name):
    """
    	param image_path: 上传图片路径
    	param file_name: 上传图片名字
    	return:
    """
    prefix = '/ai-poem/userPic/'
    secret_id = 'AKID356CMQRQQFvWzGLF3hAUegW9eO4fBbCg'  # 替换为用户的secret_id
    secret_key = 'UrBZuAZpx1vfJrPpHnaOESeunjaL3qfQ'  # 替换为用户的secret_key
    region = 'ap-nanjing'  # 替换为用户的region
    token = None  # 使用临时密钥需要传入Token，默认为空,可不填
    config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token)  # 获取配置对象
    client = CosS3Client(config)
    upload_file_name = prefix + file_name
    response = client.upload_file(
        Bucket='ai-poem-1312743943',
        Key=upload_file_name,
        PartSize=1,
        LocalFilePath=image_path,
        MAXThread=10,
        EnableMD5=False
    )
    return "https://{0}.cos.{1}.myqcloud.com/{2}".format('ai-poem-1312743943', region, upload_file_name)


# def get_base(url):
#     response = requests.get(url)
#     img_stream = base64.b64encode(response.content).decode()
#     return img_stream


def get_font(font):
    if font == "黑体":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/simhei.ttf"
    if font == "宋体":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/simsun.ttc"
    if font == "隶书":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/SIMLI.TTF"
    if font == "楷体":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/simkai.ttf"
    if font == "华文行楷":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/STXINGKA.TTF"
    if font == "华文新魏":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/STXINWEI.TTF"
    if font == "方正舒体":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/FZSTK.TTF"
    if font == "方正姚体":
        return "https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/font/FZYTK.TTF"


def __font_as_bytes(file_name):
    font_file = urllib.request.urlopen(file_name)
    font_bytes = io.BytesIO(font_file.read())
    return font_bytes


def file_load(imageId, font, color, poem, length):
    image_name = uuid.uuid4()
    response = requests.get(base_url + imageId)
    image = Image.open(BytesIO(response.content))
    # image.show()
    width, height = image.size
    # 生成一张尺寸为 width * height  背景色为白色的图片
    bg = Image.new('RGB', (width, height), color=(255, 255, 0))
    print(width, height)
    # 写入底图
    bg.paste(image, (0, 0))
    font_bytes = __font_as_bytes(get_font(font))
    font1 = ImageFont.truetype(font_bytes, 35)
    # font1 = ImageFont.load(get_font(font))
    # font1.get
    # 计算出要写入的文字占用的像素
    draw = ImageDraw.Draw(bg)
    if length == 5:
        text = " ".join(poem[0])
        w, h = font1.getsize(text)
        print(w, h)
        for i in range(len(poem)):
            text_list = re.findall(".{1}", poem[i])
            new_text = " ".join(text_list)
            print(new_text)
            draw.text(((width - w) / 2, height / 2 - h * ((11 / 4) - (i * (3 / 2)))), new_text, font=font1,
                      fill=color)
    else:
        w, h = font1.getsize(poem[0])
        for i in range(len(poem)):
            draw.text(((width - w) / 2, height / 2 - h * ((11 / 4) - (i * (3 / 2)))), poem[i], font=font1,
                      fill=color)
    # 保存
    bg.save("%s.jpg" % image_name)
    pic_url = cos_upload("%s.jpg" % image_name, "%s.jpg" % image_name)
    os.remove("%s.jpg" % image_name)

    return pic_url
