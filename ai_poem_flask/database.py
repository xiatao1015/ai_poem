import base64
import random
import pymysql
from photo import *


# 创建一个对象，设置名为db
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://mysql:mysql@8.136.2.137:3306/poem_data"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
# db = SQLAlchemy(app)

class card:
    def __init__(self, id, image, like):
        self.id = id
        self.image = image
        self.like = like
        self.ifPink = False

    def to_json(self):
        dic = self.__dict__
        return json.dumps(dic, cls=MyEncoder, ensure_ascii=False)


class Database:
    def __init__(self):
        self.connection = pymysql.connect(database='poem_data', host='8.136.2.137', port=3306, user='root',
                                          password='root')
        self.cursor = self.connection.cursor()

    def insert_image(self, image, like):
        sql = """insert into card(picture,`like`,now_time) values(%s,%s,now())"""
        imagee = (image, like)
        self.cursor.execute(sql, imagee)
        self.connection.commit()

    def get_image(self, id):
        sql = """select * from card where id= '%s'""" % id
        self.cursor.execute(sql)
        image = self.cursor.fetchone()[1]
        img = base64.b64decode(image)
        ima = Image.open(BytesIO(img))
        return ima

    # def get_image_bc(self, id):
    #     sql = """select * from back_img where id= '%s' """ % id
    #     print(sql)
    #     self.cursor.execute(sql)
    #     image = self.cursor.fetchone()[1]
    #     img = base64.b64decode(image)
    #     ima = Image.open(BytesIO(img))
    #     return ima

    def get_best_card(self):
        sql = """select * from card order by `like` desc limit 12"""
        print(sql)
        self.cursor.execute(sql)
        card_list = []
        for (id, picture, like, now_time) in self.cursor:
            card1 = card(id, picture, like)
            card2 = card1.to_json()
            card_list.append(card2)
        return card_list

    def get_new_card(self):
        sql = """select * from card order by now_time desc limit 12"""
        print(sql)
        self.cursor.execute(sql)
        card_list = []
        for (id, picture, like, now_time) in self.cursor:
            card1 = card(id, picture, like)
            card2 = card1.to_json()
            card_list.append(card2)
        return card_list

    def get_random_card(self):
        sql = """select max(id) from card"""
        self.cursor.execute(sql)
        maxid = self.cursor.fetchone()[0]
        card_list = []
        for i in range(1, 13):
            id = random.randint(5, maxid)
            sql1 = """select * from card where id= %s""" % id
            self.cursor.execute(sql1)
            for (id, picture, like, now_time) in self.cursor:
                card1 = card(id, picture, like)
                card2 = card1.to_json()
                card_list.append(card2)
        return card_list

    def update_num(self, id, like):
        sql = """update card set `like`=(%s) where id=(%s)"""
        update = (like, id)
        self.cursor.execute(sql, update)
        self.connection.commit()
        return "success"
# 创建orm模型---
# class card(db.Model):
#     __table name__ = 'card'  # 设置表名
#     id = db.Column(db.Integer, primary_key=True)
#     image = db.Column(db.BLOB, nullable=False)
#     number = db.Column(db.Integer, nullable=False)
#     db.create_all()
# def insert_card():
#
#     # 数据添加
#     db.session.add()
#     # 数据提交
#     db.session.commit()
#     # 最后进行返回操作
#     return "数据操作成功"
#
#     # 2.查询数据
#     # filter_by() 返回一个类列表的对象
#     article = mmm.query.filter_by(id=1)[0]
#     print(article.title)  # 将id为1的title取出来
#     return "数据操作成功"
#
#     # 3.修改数据
#     article = mmm.query.filter_by(id=1)[0]
#     article.content = "xxx"  # 将id为一的content的数据改成xxx
#     # 修改数据的进行数据提交
#     db.session.commit
#     return "数据操作成功"
#
#     # 4.删除数据
#     mmm.query.filter_by(id=1).delete()
#     db.session.commit()
#     return "数据操作成功"
