import jieba.analyse


class Text2Poem(object):
    def __init__(self, content):
        self.content = content

    def extract_keys(self):
        result = jieba.analyse.extract_tags(self.content, topK=4)
        return result