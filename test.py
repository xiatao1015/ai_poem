# 使用 pip 安装sdk：pip install -U cos-python-sdk-v5

# -*- coding=utf-8
from qcloud_cos import CosConfig
from qcloud_cos import CosS3Client


def cos_upload(image_path, file_name):
    """
    	:param image_path: 上传图片路径
    	:param file_name: 上传图片名字
    	:return:
    """
    prefix = '/ai-poem/userPic/'
    secret_id = 'AKID356CMQRQQFvWzGLF3hAUegW9eO4fBbCg'  # 替换为用户的secret_id
    secret_key = 'UrBZuAZpx1vfJrPpHnaOESeunjaL3qfQ'  # 替换为用户的secret_key
    region = 'ap-nanjing'  # 替换为用户的region
    token = None  # 使用临时密钥需要传入Token，默认为空,可不填
    config = CosConfig(Region=region, SecretId=secret_id, SecretKey=secret_key, Token=token)  # 获取配置对象
    client = CosS3Client(config)
    upload_file_name = prefix + file_name
    response = client.upload_file(
        Bucket='ai-poem-1312743943',
        Key=upload_file_name,
        PartSize=1,
        LocalFilePath=image_path,
        MAXThread=10,
        EnableMD5=False
    )
    print(response)
    return "https://{0}.cos.{1}.myqcloud.com/{2}".format('ai-poem-1312743943', region, upload_file_name)