from MyQR import myqr


def gen_qrcode(url, version, save_name):
    '''

    :param url: 存入二维码的连接
    :param version: 二维码大小【1-40】
    :param save_name: 二维码名字
    :param save_dir: 二维码存储路径
    :return: none
    '''
    myqr.run(
        words=url,
        version=version,
        level='H',
        save_name=save_name
    )


if __name__ == '__main__':
    gen_qrcode('https://ai-poem-1312743943.cos.ap-nanjing.myqcloud.com/ai-poem/userPic/test.png',
               20,
               'test.png')